﻿using System.Collections.Generic;

namespace KeyLockr.Models
{
    public class Result
    {
        public List<string> Messages { get; set; }
        public bool Success { get; set; }

        public Result()
        {
            this.Messages = new List<string>();
        }
    }

    public class Result<T> where T : class
    {
        public T Data { get; set; }
        public List<string> Messages { get; set; }
        public bool Success { get; set; }

        public Result()
        {
            this.Messages = new List<string>();
        }
    }
}
