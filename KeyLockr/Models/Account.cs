﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using KeyLockr.Interfaces;

namespace KeyLockr.Models
{
    public class Account : IAccount
    {
        [Key]
        [Required(ErrorMessage = "Id is required")]
        public Guid Id { get; set; } //NOTE - If this was on a server/database it would be an int but Guids are easy for testing locally - CW

        [Required(ErrorMessage = "Username is required")]
        [EmailAddress(ErrorMessage = "The email format is not valid")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [RegularExpression("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{6,20}$")]
        public string Password { get; set; }

        public List<BiometricDevice> BiometricDevices { get; set; }

        public List<SensitiveInfo> SensitiveInfos { get; set; } 

        public DateTime Attempttime { get; set; }

        public int AttemptCount { get; set; }

        public Account() {}

        public Account(string username, string password)
        {
            this.Id = Guid.NewGuid();

            if (!string.IsNullOrEmpty(username))
            {
                this.Username = username;
            }

            if (!string.IsNullOrEmpty(password))
            {
                this.Password = password;
            }

            this.BiometricDevices = new List<BiometricDevice>();
            this.SensitiveInfos = new List<SensitiveInfo>();
        }
    }
}
