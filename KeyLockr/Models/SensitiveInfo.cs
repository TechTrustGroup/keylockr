﻿using System;
using KeyLockr.Interfaces;

namespace KeyLockr.Models
{
    public class SensitiveInfo : ISensitiveInfo
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Url { get; set; }

        public SensitiveInfo() {}

        public SensitiveInfo(string title, string username, string password, string url)
        {
            this.Id = new Guid();

            if (!string.IsNullOrEmpty(title))
            {
                this.Title = title;
            }

            if (!string.IsNullOrEmpty(username))
            {
                this.Username = username;
            }

            if (!string.IsNullOrEmpty(password))
            {
                this.Password = password;
            }

            if (!string.IsNullOrEmpty(url))
            {
                this.Url = url;
            }
        }
    }
}
