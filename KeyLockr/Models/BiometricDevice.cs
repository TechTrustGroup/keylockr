﻿using System;
using KeyLockr.Interfaces;

namespace KeyLockr.Models
{
    public class BiometricDevice : IBiometricDevice
    {
        public Guid DeviceId { get; set; }
        public byte[] PublicKey { get; set; }
        //The KeyAttestationResult is only used if the User device has a TPM (Trusted Platform Module) chip, in most cases it will not.
        //public KeyCredentialAttestationResult KeyAttestationResult { get; set; }
    }
}
