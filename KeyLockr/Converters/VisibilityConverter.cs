﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace KeyLockr.Converters
{
    public class VisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            bool flag = false;
            if (value is bool)
            {
                flag = (bool)value;
            }
            else if (value is bool?)
            {
                bool? nullable = (bool?)value;
                flag = nullable.GetValueOrDefault();
            }
            else if (value is int)
            {
                flag = (int)value > 0;
            }
            else if (value is string)
            {
                flag = !String.IsNullOrEmpty((string)value);
            }
            else if (value is TimeSpan)
            {
                flag = TimeSpan.Zero != (TimeSpan)value;
            }
            else if (value != null && value != DependencyProperty.UnsetValue)
            {
                flag = true;
            }

            if (parameter != null)
            {
                if (parameter is string
                    && parameter.ToString() == "!")
                {
                    flag = !flag;
                }
            }

            if (flag)
            {
                return Visibility.Visible;
            }
            else
            {
                return Visibility.Collapsed;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
