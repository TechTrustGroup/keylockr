﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Windows.Storage;
using KeyLockr.Models;

namespace KeyLockr.Helpers
{
    public static class FileUtilsHelper
    {
        private const string USER_ACCOUNT_LIST_FILE_NAME = "localAccountsList.txt";
        //This cannot be a const because the LocalFolder is accessed at runtime - CW
        private static readonly string _localAccountsFilepath = Path.Combine(ApplicationData.Current.LocalFolder.Path, USER_ACCOUNT_LIST_FILE_NAME);

        /// <summary>
        /// Create and save a useraccount list file. (Replacing the old one)
        /// </summary>
        public static async void SaveAccountListAsync(List<Account> accounts)
        {
            string accountsXml = SerializeAccountListToXml(accounts);

            if (File.Exists(_localAccountsFilepath))
            {
                StorageFile accountsFile = await StorageFile.GetFileFromPathAsync(_localAccountsFilepath);
                await FileIO.WriteTextAsync(accountsFile, accountsXml);
            }
            else
            {
                StorageFile accountsFile = await ApplicationData.Current.LocalFolder.CreateFileAsync(USER_ACCOUNT_LIST_FILE_NAME);
                await FileIO.WriteTextAsync(accountsFile, accountsXml);
            }
        }

        /// <summary>
        /// Gets the useraccount list file and deserializes it from XML to a list of useraccount objects.
        /// </summary>
        /// <returns>List of useraccount objects</returns>
        public static async Task<List<Account>> LoadAccountListAsync()
        {
            if (File.Exists(_localAccountsFilepath))
            {
                StorageFile accountsFile = await StorageFile.GetFileFromPathAsync(_localAccountsFilepath);

                string accountsXml = await FileIO.ReadTextAsync(accountsFile);
                List<Account> accounts = DeserializeXmlToAccountList(accountsXml);
                return accounts;
            }

            return new List<Account>();
        }

        /// <summary>
        /// Uses the local list of accounts and returns an XML formatted string representing the list
        /// </summary>
        /// <returns>XML formatted list of accounts</returns>
        public static string SerializeAccountListToXml(List<Account> accounts)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<Account>));
            StringWriter writer = new StringWriter();
            xmlSerializer.Serialize(writer, accounts);
            return writer.ToString();
        }

        /// <summary>
        /// Takes an XML formatted string representing a list of accounts and returns a list object of accounts
        /// </summary>
        /// <param name="listAsXml">XML formatted list of accounts</param>
        /// <returns>List object of accounts</returns>
        public static List<Account> DeserializeXmlToAccountList(string listAsXml)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<Account>));
            TextReader textreader = new StreamReader(new MemoryStream(Encoding.UTF8.GetBytes(listAsXml)));
            return (xmlSerializer.Deserialize(textreader)) as List<Account>;
        }
    }
}
