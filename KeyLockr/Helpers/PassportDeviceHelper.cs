﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Security.Credentials;
using Windows.Security.Cryptography;
using Windows.Security.ExchangeActiveSyncProvisioning;
using Windows.Storage.Streams;
using KeyLockr.Interfaces;
using KeyLockr.Services;

namespace KeyLockr.Helpers
{
    public static class PassportDeviceHelper
    {
        public static Guid GetDeviceId()
        {
            //Get the Device ID to pass to the server
            EasClientDeviceInformation deviceInformation = new EasClientDeviceInformation();
            return deviceInformation.Id;
        }

        /// <summary>
        /// Checks to see if Passport is ready to be used.
        /// 
        /// Passport has dependencies on:
        ///     1. Having a connected Microsoft Account
        ///     2. Having a Windows PIN set up for that _account on the local machine
        /// </summary>
        public static async Task<bool> MicrosoftPassportAvailableCheckAsync()
        {
            bool keyCredentialAvailable = await KeyCredentialManager.IsSupportedAsync();
            if (keyCredentialAvailable == false)
            {
                // Key credential is not enabled yet as user 
                // needs to connect to a Microsoft Account and select a PIN in the connecting flow.
                Debug.WriteLine("Microsoft Passport is not setup!\nPlease go to Windows Settings and set up a PIN to use it.");
                return false;
            }

            return true;
        }

        /// <summary>
        /// Creates a Passport key on the machine using the _account id passed.
        /// Will also attempt to create an attestation that this key is backed by hardware on the device, but is not a requirement
        /// for a working key in this scenario. It is possible to not accept a key that is software-based only.
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="username"></param>
        /// <returns>Boolean representing if creating the Passport key succeeded</returns>
        public static async Task<bool> CreatePassportKeyAsync(Guid accountId, string username)
        {
            KeyCredentialRetrievalResult keyCreationResult = await KeyCredentialManager.RequestCreateAsync(username, KeyCredentialCreationOption.ReplaceExisting);

            switch (keyCreationResult.Status)
            {
                case KeyCredentialStatus.Success:
                    Debug.WriteLine("Successfully made key");

                    // In the real world authentication would take place on a server.
                    // So every time a user migrates or creates a new Microsoft Passport account.
                    // Passport details should be pushed to the server.
                    // The details that would be pushed to the server include:
                    // The public key, keyAttesation if available, 
                    // certificate chain for attestation endorsement key if available,  
                    // status code of key attestation result: keyAttestationIncluded or 
                    // keyAttestationCanBeRetrievedLater and keyAttestationRetryType
                    await GetKeyAttestationAsync(accountId, keyCreationResult);
                    return true;
                case KeyCredentialStatus.UserCanceled:
                    Debug.WriteLine("User cancelled sign-in process.");
                    break;
                case KeyCredentialStatus.NotFound:
                    // User needs to setup Microsoft Passport
                    Debug.WriteLine("Microsoft Passport is not setup!\nPlease go to Windows Settings and set up a PIN to use it.");
                    break;
                default:
                    break;
            }

            return false;
        }

        /// <summary>
        /// Function to be called when user requests deleting their account.
        /// Checks the KeyCredentialManager to see if there is a Passport for the current user
        /// Then deletes the local key associated with the Passport.
        /// </summary>
        public static async void RemovePassportAccountAsync(IAccount account)
        {
            //Open the account with Passport
            KeyCredentialRetrievalResult keyOpenResult = await KeyCredentialManager.OpenAsync(account.Username);

            if (keyOpenResult.Status == KeyCredentialStatus.Success)
            {
                // Call the mock server to unregister the account
                App.StorageService.RemoveAccount(account.Id);
            }

            //Then delete the account from the machines list of Passport Accounts
            await KeyCredentialManager.DeleteAsync(account.Username);
        }

        private static async Task GetKeyAttestationAsync(Guid accountId, KeyCredentialRetrievalResult keyCreationResult)
        {
            KeyCredential userKey = keyCreationResult.Credential;
            IBuffer publicKey = userKey.RetrievePublicKey();
            KeyCredentialAttestationResult keyAttestationResult = await userKey.GetAttestationAsync();
            IBuffer keyAttestation = null;
            IBuffer certificateChain = null;
            bool keyAttestationIncluded = false;
            bool keyAttestationCanBeRetrievedLater = false;
            KeyCredentialAttestationStatus keyAttestationRetryType = 0;

            if (keyAttestationResult.Status == KeyCredentialAttestationStatus.Success)
            {
                keyAttestationIncluded = true;
                keyAttestation = keyAttestationResult.AttestationBuffer;
                certificateChain = keyAttestationResult.CertificateChainBuffer;
                Debug.WriteLine("Successfully made key and attestation");
            }
            else if (keyAttestationResult.Status == KeyCredentialAttestationStatus.TemporaryFailure)
            {
                keyAttestationRetryType = KeyCredentialAttestationStatus.TemporaryFailure;
                keyAttestationCanBeRetrievedLater = true;
                Debug.WriteLine("Successfully made key but not attestation");
            }
            else if (keyAttestationResult.Status == KeyCredentialAttestationStatus.NotSupported)
            {
                keyAttestationRetryType = KeyCredentialAttestationStatus.NotSupported;
                keyAttestationCanBeRetrievedLater = false;
                Debug.WriteLine("Key created, but key attestation not supported");
            }


            Guid deviceId = GetDeviceId();
            //Update the Pasport details with the information we have just gotten above.
            App.StorageService.AddBiometricDeviceToAccount(accountId, deviceId, publicKey.ToArray(), keyAttestationResult);
        }

        public static bool UpdatePassportDetails(Guid userId, Guid deviceId, byte[] publicKey, KeyCredentialAttestationResult keyAttestationResult)
        {
            //In the real world you would use an API to add Passport signing info to server for the signed in _account.
            //For this tutorial we do not implement a WebAPI for our server and simply mock the server locally 
            //The CreatePassportKey method handles adding the Microsoft Passport account locally to the device using the KeyCredential Manager

            //Using the userId the existing account should be found and updated.
            //AuthService.AuthService.Instance.PassportUpdateDetails(userId, deviceId, publicKey, keyAttestationResult); //TODO
            return true;
        }

        public static void RemovePassportDevice(IAccount account, Guid deviceId)
        {
            App.StorageService.RemoveBiometricDeviceFromAccount(account.Id, deviceId);
        }

        /// <summary>
        /// Attempts to sign a message using the Passport key on the system for the accountId passed.
        /// </summary>
        /// <returns>Boolean representing if creating the Passport authentication message succeeded</returns>
        public static async Task<bool> GetPassportAuthenticationMessageAsync(IAccount account)
        {
            KeyCredentialRetrievalResult openKeyResult = await KeyCredentialManager.OpenAsync(account.Username);
            //Calling OpenAsync will allow the user access to what is available in the app and will not require Passport details again.
            //If you wanted to force the user to sign in again you can use the following:
            //var consentResult = await Windows.Security.Credentials.UI.UserConsentVerifier.RequestVerificationAsync(account.Username);
            //This will ask for the either the password of the currently signed in Microsoft Account or the PIN used for Microsoft Passport.

            if (openKeyResult.Status == KeyCredentialStatus.Success)
            {
                //If OpenAsync has succeeded, the next thing to think about is whether the client application requires access to backend services.
                //If it does here you would Request a challenge from the Server. The client would sign this challenge and the server
                //would check the signed challenge. If it is correct it would allow the user access to the backend.
                return await RequestSignAsync(account.Id, openKeyResult);
            }
            else if (openKeyResult.Status == KeyCredentialStatus.NotFound)
            {
                //If the _account is not found at this stage. It could be one of two errors. 
                //1. Microsoft Passport has been disabled
                //2. Microsoft Passport has been disabled and re-enabled cause the Microsoft Passport Key to change.
                //Calling CreatePassportKey and passing through the account will attempt to replace the existing Microsoft Passport Key for that account.
                //If the error really is that Microsoft Passport is disabled then the CreatePassportKey method will output that error.
                if (await CreatePassportKeyAsync(account.Id, account.Username))
                {
                    //If the Passport Key was again successfully created, Microsoft Passport has just been reset.
                    //Now that the Passport Key has been reset for the _account retry sign in.
                    return await GetPassportAuthenticationMessageAsync(account);
                }
            }

            // Can't use Passport right now, try again later
            return false;
        }

        private static async Task<bool> RequestSignAsync(Guid userId, KeyCredentialRetrievalResult openKeyResult)
        {
            //Calling userKey.RequestSignAsync() prompts the uses to enter the PIN or use Biometrics (Windows Hello).
            //The app would use the private key from the user account to sign the sign-in request (challenge)
            //The client would then send it back to the server and await the servers response.
            IBuffer challengeMessage = PassportRequestChallenge();
            KeyCredential userKey = openKeyResult.Credential;
            KeyCredentialOperationResult signResult = await userKey.RequestSignAsync(challengeMessage);

            if (signResult.Status == KeyCredentialStatus.Success)
            {
                //If the challenge from the server is signed successfully
                //send the signed challenge back to the server and await the servers response
                return SendServerSignedChallenge(userId, GetDeviceId(), signResult.Result.ToArray());
            }
            else if (signResult.Status == KeyCredentialStatus.UserCanceled)
            {
                // User cancelled the Passport PIN entry.
            }
            else if (signResult.Status == KeyCredentialStatus.NotFound)
            {
                // Must recreate Passport key
            }
            else if (signResult.Status == KeyCredentialStatus.SecurityDeviceLocked)
            {
                // Can't use Passport right now, remember that hardware failed and suggest restart
            }
            else if (signResult.Status == KeyCredentialStatus.UnknownError)
            {
                // Can't use Passport right now, try again later
            }

            return false;
        }

        public static IBuffer PassportRequestChallenge()
        {
            return CryptographicBuffer.ConvertStringToBinary("ServerChallenge", BinaryStringEncoding.Utf8);
        }

        public static bool SendServerSignedChallenge(Guid userId, Guid deviceId, byte[] signedChallenge)
        {
            //Depending on your company polices and procedures this step will be different
            //It is at this point you will need to validate the signedChallenge that is sent back from the client.
            //Validation is used to ensure the correct user is trying to access this account. 
            //The validation process will use the signedChallenge and the stored PublicKey 
            //for the username and the specific device signin is called from.
            //Based on the validation result you will return a bool value to allow access to continue or to block the account.

            //For this sample validation will not happen as a best practice solution does not apply and will need to be configured for each company.
            //Simply just return true.

            //You could get the User's Public Key with something similar to the following:
            byte[] userPublicKey = App.StorageService.GetPublicKey(userId, deviceId);
            return true;
        }
    }
}
