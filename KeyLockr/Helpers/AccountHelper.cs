﻿using System.Text.RegularExpressions;
using KeyLockr.Models;

namespace KeyLockr.Helpers
{
    public static class AccountHelper
    {
        public static Result CheckCredentialsAreValid(string username, string password)
        {
            Result result = new Result();

            // Check Username(Email) and Password for null before doing validation with Regex
            if (string.IsNullOrEmpty(username)
                || !Regex.IsMatch(username, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$"))
            {
                result.Success = false;
                result.Messages.Add("Invalid Email");
                return result;
            }

            if (string.IsNullOrEmpty(password)
                || !Regex.IsMatch(password, "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{6,20}$"))
            {
                result.Success = false;
                result.Messages.Add("Invalid Password - Please match 3 or more of the following: Lowercase letterm Uppercase letter, Numeric character, Special characters");
                return result;
            }

            result.Success = true;
            return result;
        }
    }
}
