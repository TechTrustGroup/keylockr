﻿using System;
using Windows.Security.Credentials;
using Windows.UI.Xaml;
using KeyLockr.Interfaces;
using KeyLockr.Models;
using KeyLockr.Services;

namespace KeyLockr.Controllers
{
    public class AccountController : IAccountController
    {
        public AccountController() { }

        public Result<Account> CreateAccount(string username, string password)
        {
            return App.StorageService.AddAccount(username, password);
        }

        public Result DeleteAccount(Guid accountId)
        {
            return App.StorageService.RemoveAccount(accountId);
        }

        public Result UpdateAccount(Account account)
        {
            return App.StorageService.UpdateAccount(account);
        }

        public Result<Account> CheckCredentialsExist(string username, string password)
        {
            return App.StorageService.CheckCredentialsExist(username, password);
        }

        public Result AddBiometricDeviceToAccount(Guid accountId, Guid deviceId, byte[] publicKey, KeyCredentialAttestationResult keyAttestationResult)
        {
            return App.StorageService.AddBiometricDeviceToAccount(accountId, deviceId, publicKey, keyAttestationResult);
        }

        public Result RemoveBiometricDeviceFromAccount(Guid accountId, Guid deviceId)
        {
            return App.StorageService.RemoveBiometricDeviceFromAccount(accountId, deviceId);
        }

        public Result UpdateBiometricDevice(Guid accountId, Guid deviceId, byte[] publicKey, KeyCredentialAttestationResult keyAttestationResult)
        {
            return App.StorageService.UpdateBiometricDevice(accountId, deviceId, publicKey, keyAttestationResult);
        }

        public Result CheckBiometricDeviceExists(Guid accountId, Guid deviceId)
        {
            return App.StorageService.CheckBiometricDeviceExists(accountId, deviceId);
        }
    }
}
