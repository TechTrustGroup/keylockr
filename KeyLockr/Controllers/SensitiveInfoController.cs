﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KeyLockr.Interfaces;
using KeyLockr.Models;
using KeyLockr.Services;

namespace KeyLockr.Controllers
{
    public class SensitiveInfoController : ISensitiveInfoController
    {
        public Result CreateSensitiveInfo(Guid accountId, string title, string username, string password, string url)
        {
            return App.StorageService.CreateSensitiveInfo(accountId, title, username, password, url);
        }

        public Result DeleteSensitiveInfo(int sensitiveInfoId)
        {
            throw new NotImplementedException();
        }

        public Result<SensitiveInfo> EditSensitiveInfo(SensitiveInfo sensitiveInfo)
        {
            throw new NotImplementedException();
        }
    }
}
