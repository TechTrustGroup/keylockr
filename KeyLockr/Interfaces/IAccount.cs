﻿using System;
using System.Collections.Generic;
using KeyLockr.Models;

namespace KeyLockr.Interfaces
{
    public interface IAccount
    {
        Guid Id { get; set; }
        string Username { get; set; }
        string Password { get; set; }
        List<BiometricDevice> BiometricDevices { get; set; }
        List<SensitiveInfo> SensitiveInfos { get; set; }
        //DateTime Attempttime { get; set; }
        //int AttemptCount { get; set; }
    }
}
