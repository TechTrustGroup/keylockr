﻿using System;

namespace KeyLockr.Interfaces
{
    public interface ISensitiveInfo
    {
        Guid Id { get; set; }
        string Title { get; set; }
        string Username { get; set; }
        string Password { get; set; }
        string Url { get; set; }
    }
}
