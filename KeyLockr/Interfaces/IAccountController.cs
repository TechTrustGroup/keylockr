﻿using System;
using Windows.Security.Credentials;
using KeyLockr.Models;

namespace KeyLockr.Interfaces
{
    public interface IAccountController
    {
        //Account Related
        Result<Account> CreateAccount(string username, string password); //TODO - REVIEW - this really should sent through an encrypted byte array - CW

        Result DeleteAccount(Guid accountId);

        Result UpdateAccount(Account account); //TODO - REVIEW - Not sure if it needs to pass up the entire account model - CW

        Result<Account> CheckCredentialsExist(string username, string password);

        //Biometric Device Related
        Result AddBiometricDeviceToAccount(Guid accountId, Guid deviceId, byte[] publicKey, KeyCredentialAttestationResult keyAttestationResult);

        Result RemoveBiometricDeviceFromAccount(Guid accountId, Guid deviceId);

        Result UpdateBiometricDevice(Guid accountId, Guid deviceId, byte[] publicKey, KeyCredentialAttestationResult keyAttestationResult);
        
        Result CheckBiometricDeviceExists(Guid accountId, Guid deviceId);
    }
}
