﻿using System;
using Windows.Security.Credentials;
using KeyLockr.Models;

namespace KeyLockr.Interfaces
{
    public interface IStorageService
    {
        //Account Related
        Result<Account> AddAccount(string username, string password); //TODO - REVIEW - this really should sent through an encrypted byte array - CW

        Result RemoveAccount(Guid accountId);

        Result UpdateAccount(Account account); //TODO - REVIEW - Not sure if it needs to pass up the entire account model - CW

        Result<Account> CheckCredentialsExist(string username, string password);

            //Biometric Device Related
        Result AddBiometricDeviceToAccount(Guid userId, Guid deviceId, byte[] publicKey, KeyCredentialAttestationResult keyAttestationResult);

        Result RemoveBiometricDeviceFromAccount(Guid accountId, Guid deviceId);
        Result UpdateBiometricDevice(Guid userId, Guid deviceId, byte[] publicKey, KeyCredentialAttestationResult keyAttestationResult);

        Result CheckBiometricDeviceExists(Guid accountId, Guid deviceId);

        //Passport Specific Methods
        byte[] GetPublicKey(Guid userId, Guid deviceId);

        void PassportUpdateDetails(Guid userId, Guid deviceId, byte[] publicKey,
            KeyCredentialAttestationResult keyAttestationResult);

        Result CreateSensitiveInfo(Guid accountId, string title, string username, string password, string url);
    }
}
