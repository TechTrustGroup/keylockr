﻿using System;

namespace KeyLockr.Interfaces
{
    public interface IBiometricDevice
    {
        Guid DeviceId { get; set; }

        byte[] PublicKey { get; set; }

        //KeyCredentialAttestationResult KeyAttestationResult { get; set; }
    }
}
