﻿using System;
using Windows.Devices.Sensors;
using KeyLockr.Models;

namespace KeyLockr.Interfaces
{
    public interface ISensitiveInfoController
    {
        Result CreateSensitiveInfo(Guid accountId, string title, string username, string password, string url);

        Result DeleteSensitiveInfo(int sensitiveInfoId);

        Result<SensitiveInfo> EditSensitiveInfo(SensitiveInfo sensitiveInfo);
    }
}