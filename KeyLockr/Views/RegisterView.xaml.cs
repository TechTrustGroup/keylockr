﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using KeyLockr.Controllers;
using KeyLockr.Helpers;
using KeyLockr.Interfaces;
using KeyLockr.Models;

namespace KeyLockr.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class RegisterView : Page
    {
        private IAccountController _accountController;

        public string ErrorMessage
        {
            get { return (string)this.GetValue(ErrorMessageProperty); }
            set { this.SetValue(ErrorMessageProperty, value); }
        }
        public static readonly DependencyProperty ErrorMessageProperty = DependencyProperty.Register(
            "ErrorMessage", typeof(string), typeof(RegisterView), new PropertyMetadata(default(string)));

        public bool IncorrectCredentials
        {
            get { return (bool)this.GetValue(IncorrectCredentialsProperty); }
            set { this.SetValue(IncorrectCredentialsProperty, value); }
        }
        public static readonly DependencyProperty IncorrectCredentialsProperty = DependencyProperty.Register(
            "IncorrectCredentials", typeof(bool), typeof(RegisterView), new PropertyMetadata(default(bool)));

        public RegisterView()
        {
            this.InitializeComponent();
            this.Loaded += this.RegisterView_Loaded;
        }

        private void RegisterView_Loaded(object sender, RoutedEventArgs e)
        {
            this._accountController = new AccountController();
        }

        private void RegisterButton_OnClick(object sender, RoutedEventArgs e)
        {
            string username = this.EmailAddressTextBox.Text;
            string password = this.PasswordTextBox.Password;

            Result result = AccountHelper.CheckCredentialsAreValid(username, password);

            if (result.Success)
            {
                Result<Account> account = this._accountController.CreateAccount(username, password);
                //TODO - Upon registering should there be a validation from email?  (i.e Activate account)- CW
                //TODO - This should prompt the user and setup facial recognition here - CW
                if (account.Success)
                {
                    this.Frame.Navigate(typeof (MySensitiveInfoView));
                    return;
                }
                else
                {
                    result.Messages.AddRange(account.Messages);
                }
            }

            this.IncorrectCredentials = true;
            this.ErrorMessage = string.Join(this.ErrorMessage, result.Messages.ToArray());
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {

            this.Frame.GoBack();
        }
    }
}
