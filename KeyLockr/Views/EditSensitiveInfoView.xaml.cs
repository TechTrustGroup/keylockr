﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace KeyLockr.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class EditSensitiveInfoView : Page
    {
        public EditSensitiveInfoView()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            Button selectedButton = e.Parameter as Button;
            this.DataContext = selectedButton?.DataContext;
        }

        private void EditSensitiveInfoButton_OnClick(object sender, RoutedEventArgs e)
        {
            
        }
    }
}
