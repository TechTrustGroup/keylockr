﻿using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;

namespace KeyLockr.Views
{
    /// <summary>
    /// This page is a simple UI that acts as a home screen allowing the user to click login and proceed to a login page.
    /// </summary>
    public sealed partial class HomeScreenView : Page
    {
        public HomeScreenView()
        {
            this.InitializeComponent();
        }

        private void Logo_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(LoginView));
        }

        private void RegisterButtonTextBlock_OnPointerPressed(object sender, PointerRoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(RegisterView));
        }

        private void RegisterButtonTextBlock_OnPointerEntered(object sender, PointerRoutedEventArgs e)
        {
            this.RegisterButtonTextBlock.Foreground = new SolidColorBrush(Colors.Blue);
        }

        private void RegisterButtonTextBlock_OnPointerExited(object sender, PointerRoutedEventArgs e)
        {
            this.RegisterButtonTextBlock.Foreground = new SolidColorBrush(Colors.DodgerBlue);
        }    
    }
}
