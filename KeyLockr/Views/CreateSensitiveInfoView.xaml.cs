﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using KeyLockr.Controllers;
using KeyLockr.Interfaces;
using KeyLockr.Models;

namespace KeyLockr.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class CreateSensitiveInfoView : Page
    {
        private readonly ISensitiveInfoController _sensitiveInfoController;

        public CreateSensitiveInfoView()
        {
            this.InitializeComponent();
            this._sensitiveInfoController = new SensitiveInfoController();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (App.LoggedInAccount == null)
            {
                //TODO - Show and error or navigate to login screen??
            }
        }

        private void CreateSensitiveInfo_OnClick(object sender, RoutedEventArgs e)
        {
            string title = this.TitleTextBox.Text;
            string username = this.UsernameTextBox.Text;
            string password = this.PasswordTextBox.Password;
            string url = this.UrlTextBox.Text;
            
            Result result = this._sensitiveInfoController.CreateSensitiveInfo(App.LoggedInAccount.Id, title, username, password, url);

            //TODO - based on the result so message to indicate what has occurred to the user. - CW
            if (result.Success)
            {
                this.Frame.Navigate(typeof (MySensitiveInfoView));
            }
        }
    }
}
