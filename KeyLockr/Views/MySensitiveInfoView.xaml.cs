﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Windows.ApplicationModel.Store;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using KeyLockr.Helpers;
using KeyLockr.Interfaces;
using KeyLockr.Models;

namespace KeyLockr.Views
{
    /// <summary>
    /// This page will show a logged in users accounts/data and allow the creation of new accounts/data to be saved
    /// </summary>
    public sealed partial class MySensitiveInfoView : Page
    {
        public List<SensitiveInfo> SensitiveInfos
        {
            get { return (List<SensitiveInfo>) GetValue(SensitiveInfosProperty); }
            set { SetValue(SensitiveInfosProperty, value); }
        }   
        public static readonly DependencyProperty SensitiveInfosProperty = DependencyProperty.Register(
            "SensitiveInfos", typeof (List<SensitiveInfo>), typeof (MySensitiveInfoView), new PropertyMetadata(default(List<SensitiveInfo>)));

        public MySensitiveInfoView()
        {
            this.InitializeComponent();
            this.DataContext = this;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (App.LoggedInAccount != null)
            {
                this.SensitiveInfos = App.LoggedInAccount.SensitiveInfos;
            }
        }

        private void AddNewSensitiveInfo_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof (CreateSensitiveInfoView));
        }

        private void SensitiveInfoButton_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof (EditSensitiveInfoView), sender);
        }
    }
}
