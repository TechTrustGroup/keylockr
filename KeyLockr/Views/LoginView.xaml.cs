﻿using System;
using System.Diagnostics;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using KeyLockr.Controllers;
using KeyLockr.Helpers;
using KeyLockr.Interfaces;
using KeyLockr.Models;
using System.Linq;

namespace KeyLockr.Views
{
    /// <summary>
    /// This page will allow a user to sign in with an email address and password.
    /// Once this has been done then biometrics will be used for all logins going forward.
    /// </summary>
    public sealed partial class LoginView : Page
    {
        private IAccountController _accountController;

        public string ErrorMessage
        {
            get { return (string) this.GetValue(ErrorMessageProperty); }
            set { this.SetValue(ErrorMessageProperty, value); }
        }
        public static readonly DependencyProperty ErrorMessageProperty = DependencyProperty.Register(
            "ErrorMessage", typeof (string), typeof (LoginView), new PropertyMetadata(default(string)));

        public bool IncorrectCredentials
        {
            get { return (bool) this.GetValue(IncorrectCredentialsProperty); }
            set { this.SetValue(IncorrectCredentialsProperty, value); }
        }
        public static readonly DependencyProperty IncorrectCredentialsProperty = DependencyProperty.Register(
            "IncorrectCredentials", typeof (bool), typeof (LoginView), new PropertyMetadata(default(bool)));

        public bool BiometricDeviceStatus
        {
            get { return (bool) this.GetValue(BiometricDeviceStatusProperty); }
            set { this.SetValue(BiometricDeviceStatusProperty, value); }
        }
        public static readonly DependencyProperty BiometricDeviceStatusProperty = DependencyProperty.Register(
            "BiometricDeviceStatus", typeof(bool), typeof(LoginView), new PropertyMetadata(null));

        public string BiometricDeviceStatusMessage
        {
            get { return (string) this.GetValue(BiometricDeviceStatusMessageProperty); }
            set { this.SetValue(BiometricDeviceStatusMessageProperty, value); }
        }
        public static readonly DependencyProperty BiometricDeviceStatusMessageProperty = DependencyProperty.Register(
           "BiometricDeviceStatusMessage", typeof(string), typeof(LoginView), new PropertyMetadata(default(string)));

        public LoginView()
        {
            this.InitializeComponent();
            this.Loaded += this.LoginView_Loaded;
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            //TODO - Should check for other bio metrics devices here as well - CW

            //Check Microsoft Passport is setup and available on this machine
            if (await PassportDeviceHelper.MicrosoftPassportAvailableCheckAsync())
            { 
                //Microsoft Passport is setup so inform the user
                this.BiometricDeviceStatus = true;
                this.BiometricDeviceStatusMessage = "Microsoft Passport is setup";
            }
            else
            {
                //Microsoft Passport is not setup so inform the user
                this.BiometricDeviceStatus = false;
                this.BiometricDeviceStatusMessage = "Microsoft Passport is not setup!\nPlease go to Windows Settings and set up a PIN to use it.";
            }
        }

        private void LoginView_Loaded(object sender, RoutedEventArgs routedEventArgs)
        {
            this._accountController = new AccountController();
        }

        private async void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            string username = this.EmailAddressTextBox.Text;
            string password = this.PasswordTextBox.Password;
            
            //TODO - server connection would go here maybe from the controller it will wait for a response that verifies the login (AuthService) - CW - Future release
            //TODO Limit login attempts - AP
            //TODO - For the ^ need to work out if the username is valid. Cause that is when the attempt limit applies. 
            Result<Account> account = this._accountController.CheckCredentialsExist(username, password);

            if (account != null 
                && account.Success)
            {
                //TODO there needs to be a two factor part here which determines based on priority what other sign in is required. 
                
                //Check the DeviceID. If the Account contains the ID then ask for biometric sign on.
                Guid deviceId = PassportDeviceHelper.GetDeviceId();
                Result passportDeviceExists = this._accountController.CheckBiometricDeviceExists(account.Data.Id, deviceId);

                //TODO - This needs to move. 
                App.LoggedInAccount = account.Data;

                if (passportDeviceExists.Success)
                {
                    //Sign in as an existing account with this device being already registered.
                    if (await PassportDeviceHelper.GetPassportAuthenticationMessageAsync(account.Data))
                    {
                        this.Frame.Navigate(typeof(MySensitiveInfoView), account.Data);
                    }
                    //TODO - If the passport device fails to work what do we do should this be an option to sign in with something else?? - CW
                }
                else
                {
                    //TODO - Fingerprint device check - CW - Iteration 4
                    //TODO - Need to prompt the users that the biometric features are not being used and that they should set this up - CW

                    if (this.BiometricDeviceStatus)
                    {
                        //Register the device to the Biometric devices in the Account model.
                        //Next log in will add it to the account. 
                        if (account.Data.Id != Guid.Empty)
                        {
                            //Now that the account exists on server try and create the necessary passport details and add them to the account
                            bool isSuccessful = await PassportDeviceHelper.CreatePassportKeyAsync(account.Data.Id, username);
                            if (isSuccessful)
                            {
                                Debug.WriteLine("Successfully signed in with Microsoft Passport!");
                                //TODO - Change this from being a debug line to an output line - CW
                                this.Frame.Navigate(typeof(MySensitiveInfoView), account.Data);
                            }
                            else
                            {
                                //The passport account creation failed.
                                //Remove the account from the server as passport details were not configured
                                this._accountController.RemoveBiometricDeviceFromAccount(account.Data.Id, deviceId);
                                this.ErrorMessage = "Biometric device login failed";
                            }
                        }
                    }
                    else
                    {
                        //TODO - Should prompt the user with a way to set up Biometric stuff - CW
                        //TODO - Really should send a two factor auth code to an email or phone of the users as a needed item for sign in - CW (Better Security this way)
                        this.Frame.Navigate(typeof (MySensitiveInfoView));
                    }
                }
            }
            else
            {
                //Account Sign in was not successful.
                this.IncorrectCredentials = true;
                if (account != null 
                    && account.Messages.Any())
                {
                    this.ErrorMessage = string.Join(", ", account.Messages.ToArray());
                }
                else
                {
                    this.ErrorMessage = "We couldn't log you in.";
                }
            }

        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            //TODO - Change to navigate back event - CW
            //this.Frame.Navigate(typeof(HomeScreenView));
            this.Frame.GoBack();
        }
    }
}
