﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using KeyLockr.Views;

namespace KeyLockr
{
    /// <summary>
    /// The MainPage handles all the SplitView (Hamburger) menu operations.
    /// It is also responsible for navigating to the Home page on loaded
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            this.Loaded += this.MainPage_Loaded;
        }

        private void MainPage_Loaded(object sender, RoutedEventArgs routedEventArgs)
        {
            this.MainFrame.Navigate(typeof(HomeScreenView));
        }

        private void HamburgerButton_Click(object sender, RoutedEventArgs e)
        {
            this.MySplitView.IsPaneOpen = !this.MySplitView.IsPaneOpen;
        }

        private void RadioButtonPaneItem_Click(object sender, RoutedEventArgs e)
        {
            RadioButton radioButton = sender as RadioButton;

            if (radioButton != null)
            {
                switch (radioButton.Tag.ToString())
                {
                    //TODO - Not sure if the Home button should always be shown - Especially when a user is signed in - CW
                    case "Home":
                        if (App.LoggedInAccount == null)
                        {
                            this.MainFrame.Navigate(typeof(HomeScreenView));
                        }
                        else
                        {
                            this.MainFrame.Navigate(typeof(MySensitiveInfoView));
                        }
                        
                        break;
                    case "Settings": //TODO - REVIEW - Should this be called 'My Account' - CW
                        this.MainFrame.Navigate(typeof(SettingsView));
                        break;
                }

                this.MySplitView.IsPaneOpen = false;
            }
        }
    }
}
