﻿using System;
using System.Collections.Generic;
using System.Linq;
using Windows.Security.Credentials;
using KeyLockr.Helpers;
using KeyLockr.Interfaces;
using KeyLockr.Models;

namespace KeyLockr.Services
{
    public class StorageService : IStorageService
    {
        private List<Account> _mockDatabaseAccountsList;

        public StorageService()
        {
            this._mockDatabaseAccountsList = new List<Account>();
            this.InitializeLocalAccountList();
        }

        private async void InitializeLocalAccountList()
        {
            this._mockDatabaseAccountsList = await FileUtilsHelper.LoadAccountListAsync();
        }

        public Result<Account> AddAccount(string username, string password)
        {
            Result<Account> account = new Result<Account>
            {
                Data = new Account(username, password)
            };

            this._mockDatabaseAccountsList.Add(account.Data);
            FileUtilsHelper.SaveAccountListAsync(this._mockDatabaseAccountsList);

            account.Success = true;
            return account;
        }

        public Result RemoveAccount(Guid accountId)
        {
            Result result = new Result();
            if (this._mockDatabaseAccountsList.Any(f => f.Id.Equals(accountId)))
            {
                this._mockDatabaseAccountsList.Remove(this.GetAccountById(accountId));
                FileUtilsHelper.SaveAccountListAsync(this._mockDatabaseAccountsList);
                result.Success = true;
            }

            return result;
        }

        public Result UpdateAccount(Account account)
        {
            Result result = new Result();
            return result;
        }
        
        public Result<Account> CheckCredentialsExist(string username, string password)
        {
            Result<Account> result = new Result<Account>();

            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                result.Messages.Add("Username or Password cannot be empty.");
            }
            else if (this._mockDatabaseAccountsList.Any())
            {
                Account account = this._mockDatabaseAccountsList.FirstOrDefault(f => f.Username.Equals(username));
                if (account == null)
                {
                    result.Messages.Add("User does not exist, have you registered?");
                }
                else
                {
                    if (account.Password.Equals(password))
                    {
                        result.Data = this.GetAccountById(account.Id);
                    }
                    else
                    {
                        result.Messages.Add("Invalid password");
                    }
                }
            }

            if (result.Data != null)
            {
                result.Success = true;
            }

            return result;
        }

        public Result AddBiometricDeviceToAccount(Guid accountId, Guid deviceId, byte[] publicKey, KeyCredentialAttestationResult keyAttestationResult)
        {
            Result result = new Result();
            Account account = this.GetAccountById(accountId);

            if (account != null)
            {
                account.BiometricDevices.Add(new BiometricDevice()
                {
                    DeviceId = deviceId,
                    PublicKey = publicKey,
                    //KeyAttestationResult = keyAttestationResult
                });

                FileUtilsHelper.SaveAccountListAsync(this._mockDatabaseAccountsList);
                result.Success = true;
            }

            return result;
        }

        public Result RemoveBiometricDeviceFromAccount(Guid accountId, Guid deviceId)
        {
            Result result = new Result();
            return result;
        }

        public Result UpdateBiometricDevice(Guid userId, Guid deviceId, byte[] publicKey, KeyCredentialAttestationResult keyAttestationResult)
        {
            Result result = new Result();
            return result;
        }

        public Result CheckBiometricDeviceExists(Guid accountId, Guid deviceId)
        {
            Result result = new Result();
            Account account = this.GetAccountById(accountId);

            if (account != null)
            {
                if (account.BiometricDevices.Any(f => f.DeviceId.Equals(deviceId)))
                {
                    result.Success = true;
                }
            }

            return result;
        }

        private Account GetAccountById(Guid accountId)
        {
            return this._mockDatabaseAccountsList.FirstOrDefault(f => f.Id.Equals(accountId));
        }


        //Microsoft Passport Specific Code
        public byte[] GetPublicKey(Guid userId, Guid deviceId)
        {
            IAccount account = this._mockDatabaseAccountsList.FirstOrDefault(f => f.Id.Equals(userId));
            if (account != null)
            {
                if (account.BiometricDevices.Any())
                {
                    return account.BiometricDevices.FirstOrDefault(p => p.DeviceId.Equals(deviceId)).PublicKey;
                }
            }
            return null;
        }

        public void PassportUpdateDetails(Guid userId, Guid deviceId, byte[] publicKey, KeyCredentialAttestationResult keyAttestationResult)
        {
            //Account existingUserAccount = this.GetAccountById(userId);
            //if (existingUserAccount != null)
            //{
            //    if (!existingUserAccount.BiometricDevices.Any(f => f.DeviceId.Equals(deviceId)))
            //    {
            //        existingUserAccount.BiometricDevices.Add(new BiometricDevice());
            //        {
            //            DeviceId = deviceId,
            //            PublicKey = publicKey,
            //            //KeyAttestationResult = keyAttestationResult,
            //        });
            //    }
            //}
            //FileUtilsHelper.SaveAccountListAsync(this._mockDatabaseAccountsList);
        }

        public Result CreateSensitiveInfo(Guid accountId, string title, string username, string password, string url)
        {
            Result result = new Result();

            Account account = this.GetAccountById(accountId);
            if (account != null)
            {
                SensitiveInfo sensitiveInfo = new SensitiveInfo(title, username, password, url);
                account.SensitiveInfos.Add(sensitiveInfo);
                result.Success = true;
                FileUtilsHelper.SaveAccountListAsync(this._mockDatabaseAccountsList);
            }

            return result;
        }
    }
}
