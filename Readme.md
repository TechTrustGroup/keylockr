## Version

PREALPHA - 0.0.1

## Synopsis

This project is a Universal Windows Application for the management of sensitive login data. Protection of data is the topmost priority with biometric 2 factor login enforced in the application login workflow.

## Usage

Users can store any text data in set of fields named Username, Password, and Link. Data in those fields can only be accessed by logging into the device securely with the required 2 factor authentication. Data is heavily encrypted on the loca machine, and in the future, in a remote location for shared access.

## Motivation

Most people have tens of logins to different websites and services that all should use unique passwords that are relatively difficult to guess by hackers. It is a near impossible tasks to keep track of all the different logins you have, and writting them down in a document or on paper is very unsafe. This project was created to address security concerns around login data. 

## Building/Running

The application can be run through any edition of Visual Studio 2015 with Windows 10 SDK installed. The build needs to be run in x86 or x64, not ARM to work on a PC. ARM only works with a connected phone or an emulator.

The final application can be sideloaded as is into a machine that has developer mode enabled, but in the future the application will be available in the Windows Store for all Windows devices.

If you are a new user you will need to register for a new account with an email that passes standard email validation and must have a complex password containing an uppercase, lowercase, special character and number and be 6 characters long.
Once you have register for a new account it will be saved locally to the machine. The login component of the application will now work with the account you just created. 

## Code

The code follows a layered architecture with inspiration from MVC, MVVM, and SOA. 

SOA elements are yet to be implemented, as all remote services are locally mocked during these early development phases.

MVVM and MVC both play a part in the presentation and service layers to interface in intuitive ways with the Windows interface paradigm, while still maintaining clear and concise code structures and patterns for easily maintainable code.

Login credentials are converted to XML and stored locally and encrypted at this point in time. Sensitive information stored in the application will also be stored locally and encrypted until remote services are created in later iterations.

Windows Hello is used to provide 2 factor authentication - this is up to the end user and may involve pin, password, facial recognition, or finger print authentication depending on what hardware is available and what the user has setup.

TOTP will eventually form a vital part of the authentication structure to improve security with token based keys, but is not implemented yet.

## Tests

Major points of the system are unit tested for regression testing and to stabalize the software as we develop and release iteratively. Our code coverage aim is 80% or higher to satsify minimum criteria for production ready code. We are currrently below this testing threshold as we have only performed an architectual spike, and not any formal coding processes as of yet.

## Contributors

Alec Pirillo 11702937
Christopher Williams 11735913
James Seaman 98118894
Salvatore Jalaty 12403782
Andreas Lengkeek 98060142
Felix Brennan-Bohm 98101951

## License

This code is by default copyright protected, and the property of the contributors only. You may not distribute or modify the source code without explicit consent from the contributors.